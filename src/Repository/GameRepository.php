<?php

namespace App\Repository;

use App\Entity\Game;
use App\Repository\Exception\CantSaveGame;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\Expr;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Game|null find($id, $lockMode = null, $lockVersion = null)
 * @method Game|null findOneBy(array $criteria, array $orderBy = null)
 * @method Game[]    findAll()
 * @method Game[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameRepository extends ServiceEntityRepository
{
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $entityManager;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Game::class);
        $this->entityManager = $this->getEntityManager();
    }

    public function save(Game $user): void
    {
        try {
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        } catch (PDOException | ORMException $exception) {
            throw new CantSaveGame();
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function getRecords(int $id): array
    {
        return $this->createQueryBuilder('g')
            ->select('g')
            ->andWhere((new Expr())->eq('g.word', ':word'))
            ->setParameter('word', $id)
            ->addOrderBy('g.score', 'DESC')
            ->addOrderBy('g.time', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }
}
