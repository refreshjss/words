<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Repository\Exception\CantSaveUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use Psr\Log\LoggerInterface;

class UserController extends AbstractController
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(UserRepository $userRepository, LoggerInterface $logger)
    {
        $this->userRepository = $userRepository;
        $this->logger = $logger;
    }

    /**
     * @Route("/", name="user")
     * @param Request $request
     * @return Response
     */
    public function initUser(Request $request): Response
    {
        if ($request->request->has('verify')) {
            return $this->verify($request);
        }
        if ($request->request->has('login')) {
            $login = $request->request->get('login');
            if ($this->userRepository->findOneBy(['login' => $login]) !== null) {
                return $this->render('user/verify.html.twig', ["login" => $login]);
            }
            $user = new User();
            $user->setLogin($login);
            $user->setAuthKey();
            setcookie('auth_key', $user->getAuthKey(), time() + 2400, '/');
            try {
                $this->userRepository->save($user);
            } catch (CantSaveUser $exception) {
                $this->logger->error($exception->getMessage(), ["login" => $login]);
                return $this->render('user/index.html.twig', ["message" => 'Упс, что-то пошло не так']);
            }
            return $this->redirectToRoute('game');
        }
        if ($request->cookies->has('auth_key')) {
            $authKey = $request->cookies->get('auth_key');
            if ($this->userRepository->findOneBy(['auth_key' => $authKey]) !== null) {
                return $this->redirectToRoute('game');
            }
            return $this->render('user/index.html.twig', [
                'message' => 'Жульничать не хорошо !!!',
            ]);
        }
        return $this->render('user/index.html.twig', [
            'message' => '',
        ]);
    }

    /**
     * @Route("/admin", name="admin")
     * @param Request $request
     * @return Response
     */
    public function admin(Request $request): Response
    {
        if ($request->cookies->has('auth_key')) {
            $authKey = $request->cookies->get('auth_key');
            if ($this->userRepository->findOneBy(['auth_key' => $authKey])) {
                return $this->redirectToRoute('adminController');
            }
            setcookie('auth_key', '', time() - 2400, '/');
        }
        if ($request->request->has('login') && $request->request->has('password')) {
            $user = $this->userRepository->findOneBy([
                'login' => $request->request->get('login'),
                'password' => $request->request->get('password')
            ]);
            if ($user !== null) {
                setcookie('auth_key', $user->getAuthKey(), 0, '/');
                return $this->redirectToRoute('adminController');
            }
            return $this->render('user/admin.html.twig', ['message' => 'Неверный логин или пароль']);
        }
        return $this->render('user/admin.html.twig', ['message' => '']);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function verify(Request $request): Response
    {
        $login = $request->request->get('login');
        if ($request->request->has('code')) {
            $code = $request->request->get('code');
            $user = $this->userRepository->findOneBy(['login' => $login]);
            if ($user !== null && substr($user->getAuthKey(), 0, 4) == $code) {
                setcookie('auth_key', $user->getAuthKey(), time() + 2400, '/');
                return $this->redirectToRoute('game');
            }
            return $this->render('user/index.html.twig', [
                'message' => 'Дружок-пирожок, такой ник уже занят попробуй ' . $login . '228',
            ]);
        }
        return $this->render('user/verify.html.twig', ["login" => $login,]);
    }

    /**
     * @Route ("/logout", name="logout")
     * @return Response
     */
    public function logout(): Response
    {
        setcookie('auth_key', '', time() - 2400, '/');
        return $this->redirectToRoute('user');
    }
}
