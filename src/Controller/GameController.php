<?php

namespace App\Controller;

use App\Entity\Game;
use App\Repository\Exception\CantSaveGame;
use App\Repository\GameRepository;
use App\Repository\PieceRepository;
use App\Repository\UserRepository;
use App\Repository\WordRepository;
use Doctrine\ORM\Query\Expr;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use DateTime;

/**
 * Class GameController
 * @package App\Controller
 * @Route ("/game")
 */
class GameController extends AbstractController
{
    /**
     * @var WordRepository
     */
    private $wordRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var \App\Entity\User|null
     */
    private $user = null;

    /**
     * @var GameRepository
     */
    private $gameRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var PieceRepository
     */
    private $pieceRepository;

    /**
     * GameController constructor.
     * @param WordRepository $wordRepository
     * @param UserRepository $userRepository
     * @param GameRepository $gameRepository
     * @param LoggerInterface $logger
     * @param PieceRepository $pieceRepository
     */
    public function __construct(
        WordRepository $wordRepository,
        UserRepository $userRepository,
        GameRepository $gameRepository,
        LoggerInterface $logger,
        PieceRepository $pieceRepository
    ) {
        $this->wordRepository = $wordRepository;
        $this->userRepository = $userRepository;
        $this->gameRepository = $gameRepository;
        $this->logger = $logger;
        $this->pieceRepository = $pieceRepository;
        if (isset($_COOKIE['auth_key'])) {
            $this->user = $userRepository->findOneBy(['authKey' => $_COOKIE['auth_key']]);
        }
    }

    /**
     * @Route("", name="game")
     */
    public function index(): Response
    {
        if (isset($this->user)) {
            $lvls = array_map(function ($word) {
                return $word->getLvl();
            }, $this->wordRepository->findAll());
            return $this->render('game/index.html.twig', [
                'user' => $this->user,
                'lvls' => $lvls
            ]);
        }
        return $this->redirectToRoute('user');
    }

    /**
     * @Route ("/save", name="saveGame")
     * @param Request $request
     * @return Response
     */
    public function saveGame(Request $request): Response
    {
        $game = new Game();
        $game->setUser($this->userRepository->findOneBy(['id' => $request->request->get('userId')]));
        $game->setDate(new DateTime('now'));
        $game->setWord($this->wordRepository->findOneBy(['id' => $request->request->get('wordId')]));
        $game->setTime(time() - $request->request->get('startTime'));
        $game->setScore($request->request->getInt('score'));
        try {
            $this->gameRepository->save($game);
        } catch (CantSaveGame $exception) {
            $this->logger->error($exception->getMessage(), [
                'user' => $game->getUser(),
                'word' => $game->getWord(),
                'date' => $game->getDate()
            ]);
        }
        return $this->redirectToRoute('game');
    }

    /**
     * @Route ("/records", name="records")
     */
    public function getRecords(): Response
    {
        $words = $this->wordRepository->findAll();
        return $this->render('game/words.html.twig', [
            'isRecords' => true,
            'user' => $this->user,
            'words' => $words
        ]);
    }

    /**
     * @Route ("/records/{word}", name="recordsForWord")
     * @param Request $request
     * @return Response
     */
    public function getRecordsByWord(Request $request): Response
    {
        $word = $request->attributes->get('word');
        $records = $this->gameRepository->getRecords($word);
        return $this->render('game/records.html.twig', [
            'user' => $this->user,
            'records' => $records
        ]);
    }

    /**
     * @Route ("/{lvl}", name="getLvl")
     * @param Request $request
     * @return Response
     */
    public function getWordsByLvl(Request $request): Response
    {
        $lvl = $request->attributes->get('lvl');
        $words = $this->wordRepository->findBy(['lvl' => $lvl]);
        return $this->render('game/words.html.twig', [
            'isRecords' => false,
            'user' => $this->user,
            'words' => $words,
            'lvl' => $lvl
        ]);
    }

    /**
     * @Route ("/{lvl}/{word}", name="getWord")
     * @param Request $request
     * @return Response
     */
    public function startGame(Request $request): Response
    {
        $wordId = $request->attributes->get('word');
        return $this->render('game/game.html.twig', [
            'user' => $this->user,
            'start_time' => time(),
            'word' => $this->wordRepository->findOneBy(['id' => $wordId])
        ]);
    }

    /**
     * @Route("/api/piece/find", name="apiPiece")
     * @param Request $request
     * @return Response
     */
    public function findPiece(Request $request): Response
    {
        $data = $request->getContent();
        $request->request->replace(json_decode((string)$data, true));
        $word = $this->wordRepository->findOneBy(['id' => $request->request->get('wordId')]);
        if ($word) {
            $piece = $this->pieceRepository->findOneBy(['name' => $request->request->get('piece')]);
            $find = false;
            foreach ($word->getPieces() as $findPiece) {
                if ($findPiece->getName() == $piece->getName()) {
                    $find = true;
                }
            }
            if ($find) {
                return new JsonResponse(['score' => $piece->getScore(), 'piece' => $piece->getName()], 200);
            }
        }
        return new JsonResponse(['message' => "Такого слова нет"], 404);
    }
}
