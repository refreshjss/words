<?php

namespace App\Form;

use App\Entity\Piece;
use App\Entity\Word;
use App\Repository\PieceRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WordType extends AbstractType
{
    /**
     * @var PieceRepository
     */
    private $pieceRepository;

    public function __construct(PieceRepository $pieceRepository)
    {
        $this->pieceRepository = $pieceRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('lvl')
            ->add('pieces', ChoiceType::class, array(
                'choices' => $this->pieceRepository->findBy(['word' => null]),
                'choice_label' => function ($piece) {
                    /** @var Piece $piece */
                    return $piece->getName();
                },
                'expanded' => true,
                'multiple' => true
            ));
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Word::class,
        ]);
    }
}
