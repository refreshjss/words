<?php

namespace App\Tests\Entity;

use PHPUnit\Framework\TestCase;
use App\Entity\Game;

class GameTest extends TestCase
{
    public function testTimeFullFormat(): void
    {
        $game = new Game();
        $game->setTime(65);
        $this->assertEquals("1:5", $game->getTime());
    }

    public function testTimeShortFormat(): void
    {
        $game = new Game();
        $game->setTime(42);
        $this->assertEquals("42", $game->getTime());
    }
}
