<?php

namespace App\Tests\Entity;

use PHPUnit\Framework\TestCase;
use App\Entity\User;

class UserTest extends TestCase
{
    public function testAuthKey(): void
    {
        $user = new User();
        $user->setLogin('reFreshjss');
        $user->setAuthKey();
        $this->assertEquals('0ac31d1f23a969c138d762ad1a77e56957839d5b', $user->getAuthKey());
    }
}
