const score = document.getElementById('score');
const findWords = document.getElementById('find');
const word = document.getElementById('wordId');
const hiddenScope = document.getElementById('hiddenScore');
const piece = document.getElementById('word');

document.getElementById('verify').addEventListener('click', function () {
    let xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://157.230.106.225:6021/game/api/piece/find', true);
    xhr.setRequestHeader("Content-Type", "application/json");
    let data = JSON.stringify({wordId: word.value, piece: piece.value});
    piece.value = "";
    xhr.addEventListener('readystatechange', function () {
        if (xhr.readyState === 4) {
            if (xhr.status !== 200) {
                let message = JSON.parse(xhr.responseText);
                alert(message['message']);
            } else {
                let result = JSON.parse(xhr.responseText);
                score.textContent = parseInt(score.textContent) + result['score'];
                hiddenScope.setAttribute('value', score.textContent);
                findWords.textContent += result['piece'] + " ";
            }
        }
    });
    xhr.send(data);
});
